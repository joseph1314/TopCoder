#include <cstdio>
#include <cmath>
#include <cstring>
#include <ctime>
#include <iostream>
#include <algorithm>
#include <set>
#include <vector>
#include <sstream>
#include <typeinfo>
#include <fstream>

using namespace std;

class ApplesAndPears {
public:
    int getArea(vector<string> board, int K) {
        int ans = 1;
        int n = board.size();
        vector<vector<int>> e(n + 1, vector<int>(n + 1, 0)), a(n + 1, vector<int>(n + 1, 0)),
            p(n + 1, vector<int>(n + 1, 0));
        int A = 0, E = 0, P = 0;
        for (int i = 1;i <= n;i++) {
            for (int j = 1;j <= n;j++) {
                e[i][j] += e[i - 1][j] + e[i][j - 1] - e[i - 1][j - 1] + (board[i - 1][j - 1] == '.');
                a[i][j] += a[i - 1][j] + a[i][j - 1] - a[i - 1][j - 1] + (board[i - 1][j - 1] == 'A');
                p[i][j] += p[i - 1][j] + p[i][j - 1] - p[i - 1][j - 1] + (board[i - 1][j - 1] == 'P');
                if (board[i - 1][j - 1] == 'A') A++;
                else if (board[i - 1][j - 1] == 'P') P++;
                else E++;
            }
        }
        for (int i = 1;i <= n;i++) {
            for (int j = 1;j <= n;j++) {
                for (int l = i;l <= n;l++) {
                    for (int r = j;r <= n;r++) {
                        int area = (l - i + 1) * (r - j + 1);
                        int t1 = (e[l][r] - e[i - 1][r] - e[l][j - 1] + e[i - 1][j - 1]);
                        int t2 = (a[l][r] - a[i - 1][r] - a[l][j - 1] + a[i - 1][j - 1]);
                        int t3 = (p[l][r] - p[i - 1][r] - p[l][j - 1] + p[i - 1][j - 1]);
                        if (E - t1 >= t2 + t3 && t2 + t3 <= K) {
                            ans = max(ans, area);
                            continue;
                        }
                        if (A >= area && (t3==0 or E>0) and area-t2+t3<=K) {
                            ans = max(ans, area);
                        }
                        if (P >= area && (t2==0 or E>0) and area-t3+t2<= K) {
                            ans = max(ans, area);
                        }
                    }
                }
            }
        }
        return ans;
        return 0;
    }
};

// CUT begin
ifstream datas("ApplesAndPears.sample");

string next_line() {
    string s;
    getline(datas, s);
    return s;
}

template <typename T> void from_stream(T& t) {
    stringstream ss(next_line());
    ss >> t;
}

void from_stream(string& s) {
    s = next_line();
}

template <typename T> void from_stream(vector<T>& ts) {
    int len;
    from_stream(len);
    ts.clear();
    for (int i = 0; i < len; ++i) {
        T t;
        from_stream(t);
        ts.push_back(t);
    }
}

template <typename T>
string to_string(T t) {
    stringstream s;
    s << t;
    return s.str();
}

string to_string(string t) {
    return "\"" + t + "\"";
}

bool do_test(vector<string> board, int K, int __expected) {
    time_t startClock = clock();
    ApplesAndPears* instance = new ApplesAndPears();
    int __result = instance->getArea(board, K);
    double elapsed = (double)(clock() - startClock) / CLOCKS_PER_SEC;
    delete instance;

    if (__result == __expected) {
        cout << "PASSED!" << " (" << elapsed << " seconds)" << endl;
        return true;
    }
    else {
        cout << "FAILED!" << " (" << elapsed << " seconds)" << endl;
        cout << "           Expected: " << to_string(__expected) << endl;
        cout << "           Received: " << to_string(__result) << endl;
        return false;
    }
}

int run_test(bool mainProcess, const set<int>& case_set, const string command) {
    int cases = 0, passed = 0;
    while (true) {
        if (next_line().find("--") != 0)
            break;
        vector<string> board;
        from_stream(board);
        int K;
        from_stream(K);
        next_line();
        int __answer;
        from_stream(__answer);

        cases++;
        if (case_set.size() > 0 && case_set.find(cases - 1) == case_set.end())
            continue;

        cout << "  Testcase #" << cases - 1 << " ... ";
        if (do_test(board, K, __answer)) {
            passed++;
        }
    }
    if (mainProcess) {
        cout << endl << "Passed : " << passed << "/" << cases << " cases" << endl;
        int T = time(NULL) - 1647839896;
        double PT = T / 60.0, TT = 75.0;
        cout << "Time   : " << T / 60 << " minutes " << T % 60 << " secs" << endl;
        cout << "Score  : " << 1000 * (0.3 + (0.7 * TT * TT) / (10.0 * PT * PT + TT * TT)) << " points" << endl;
    }
    return 0;
}

int main(int argc, char* argv[]) {
    cout.setf(ios::fixed, ios::floatfield);
    cout.precision(2);
    set<int> cases;
    bool mainProcess = true;
    for (int i = 1; i < argc; ++i) {
        if (string(argv[i]) == "-") {
            mainProcess = false;
        }
        else {
            cases.insert(atoi(argv[i]));
        }
    }
    if (mainProcess) {
        cout << "ApplesAndPears (1000 Points)" << endl << endl;
    }
    return run_test(mainProcess, cases, argv[0]);
}
// CUT end
