#include <cstdio>
#include <cmath>
#include <cstring>
#include <ctime>
#include <iostream>
#include <algorithm>
#include <set>
#include <vector>
#include <sstream>
#include <typeinfo>
#include <fstream>
using namespace std;
using ii = pair<int, int>;
class CatchTheBeatEasy {
public:
    string ableToCatchAll(vector<int> x, vector<int> y) {
        bool ok = 1;
        int xx = 0;
        vector<ii> pos;
        int n = x.size();
        for (int i = 0;i < n;i++) {
            pos.emplace_back(y[i], x[i]);
        }
        sort(pos.begin(), pos.end());
        int t = 0;
        for (int i = 0;i < n;i++) {
            int a = pos[i].first - t;
            int b = abs(xx - pos[i].second);
            if (a <= 0 && xx != pos[i].second ||(a<b)) {
                ok = 0;
                break;
            }
            else {
                xx = pos[i].second;
                t += a;
            }
        }
        if (ok) return "Able to catch";
        else return "Not able to catch";
        return "";
    }
};

// CUT begin
ifstream datas("CatchTheBeatEasy.sample");

string next_line() {
    string s;
    getline(datas, s);
    return s;
}

template <typename T> void from_stream(T& t) {
    stringstream ss(next_line());
    ss >> t;
}

void from_stream(string& s) {
    s = next_line();
}

template <typename T> void from_stream(vector<T>& ts) {
    int len;
    from_stream(len);
    ts.clear();
    for (int i = 0; i < len; ++i) {
        T t;
        from_stream(t);
        ts.push_back(t);
    }
}

template <typename T>
string to_string(T t) {
    stringstream s;
    s << t;
    return s.str();
}

string to_string(string t) {
    return "\"" + t + "\"";
}

bool do_test(vector<int> x, vector<int> y, string __expected) {
    time_t startClock = clock();
    CatchTheBeatEasy* instance = new CatchTheBeatEasy();
    string __result = instance->ableToCatchAll(x, y);
    double elapsed = (double)(clock() - startClock) / CLOCKS_PER_SEC;
    delete instance;

    if (__result == __expected) {
        cout << "PASSED!" << " (" << elapsed << " seconds)" << endl;
        return true;
    }
    else {
        cout << "FAILED!" << " (" << elapsed << " seconds)" << endl;
        cout << "           Expected: " << to_string(__expected) << endl;
        cout << "           Received: " << to_string(__result) << endl;
        return false;
    }
}

int run_test(bool mainProcess, const set<int>& case_set, const string command) {
    int cases = 0, passed = 0;
    while (true) {
        if (next_line().find("--") != 0)
            break;
        vector<int> x;
        from_stream(x);
        vector<int> y;
        from_stream(y);
        next_line();
        string __answer;
        from_stream(__answer);

        cases++;
        if (case_set.size() > 0 && case_set.find(cases - 1) == case_set.end())
            continue;

        cout << "  Testcase #" << cases - 1 << " ... ";
        if (do_test(x, y, __answer)) {
            passed++;
        }
    }
    if (mainProcess) {
        cout << endl << "Passed : " << passed << "/" << cases << " cases" << endl;
        int T = time(NULL) - 1647834513;
        double PT = T / 60.0, TT = 75.0;
        cout << "Time   : " << T / 60 << " minutes " << T % 60 << " secs" << endl;
        cout << "Score  : " << 250 * (0.3 + (0.7 * TT * TT) / (10.0 * PT * PT + TT * TT)) << " points" << endl;
    }
    return 0;
}

int main(int argc, char* argv[]) {
    cout.setf(ios::fixed, ios::floatfield);
    cout.precision(2);
    set<int> cases;
    bool mainProcess = true;
    for (int i = 1; i < argc; ++i) {
        if (string(argv[i]) == "-") {
            mainProcess = false;
        }
        else {
            cases.insert(atoi(argv[i]));
        }
    }
    if (mainProcess) {
        cout << "CatchTheBeatEasy (250 Points)" << endl << endl;
    }
    return run_test(mainProcess, cases, argv[0]);
}
// CUT end
