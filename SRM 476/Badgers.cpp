#include <cstdio>
#include <cmath>
#include <cstring>
#include <ctime>
#include <iostream>
#include <algorithm>
#include <set>
#include <vector>
#include <sstream>
#include <typeinfo>
#include <fstream>
#include <bits/stdc++.h>
using namespace std;

class Badgers
{
public:
    int feedMost(vector<int> hunger, vector<int> greed, int totalFood)
    {
        int n = hunger.size();// n <=50 
        int res = 0;
        using ii = pair<int, int>;
        vector<ii> data;
        for (int i = 0;i < n;i++) {
            data.emplace_back(hunger[i], greed[i]);
        }
        for (int k = 1;k <= n;k++) {
            sort(data.begin(), data.end(), [&](const ii& a, const ii& b) {
                return a.first + a.second * (k - 1) < b.first + b.second * (k - 1);
                });
            int x = totalFood;
            int cnt = 0;
            for (int i = 0;i < n;i++) {
                if (x < data[i].first + (k - 1) * data[i].second) break;
                else {
                    x -= data[i].first + (k - 1) * data[i].second;
                    cnt++;
                }
                if(cnt==k) break;
            }
            res = max(res, cnt);
        }
        return res;
    }
};

// CUT begin
ifstream data("Badgers.sample");

string next_line()
{
    string s;
    getline(data, s);
    return s;
}

template <typename T>
void from_stream(T& t)
{
    stringstream ss(next_line());
    ss >> t;
}

void from_stream(string& s)
{
    s = next_line();
}

template <typename T>
void from_stream(vector<T>& ts)
{
    int len;
    from_stream(len);
    ts.clear();
    for (int i = 0; i < len; ++i)
    {
        T t;
        from_stream(t);
        ts.push_back(t);
    }
}

template <typename T>
string to_string(T t)
{
    stringstream s;
    s << t;
    return s.str();
}

string to_string(string t)
{
    return "\"" + t + "\"";
}

bool do_test(vector<int> hunger, vector<int> greed, int totalFood, int __expected)
{
    time_t startClock = clock();
    Badgers* instance = new Badgers();
    int __result = instance->feedMost(hunger, greed, totalFood);
    double elapsed = (double)(clock() - startClock) / CLOCKS_PER_SEC;
    delete instance;

    if (__result == __expected)
    {
        cout << "PASSED!"
            << " (" << elapsed << " seconds)" << endl;
        return true;
    }
    else
    {
        cout << "FAILED!"
            << " (" << elapsed << " seconds)" << endl;
        cout << "           Expected: " << to_string(__expected) << endl;
        cout << "           Received: " << to_string(__result) << endl;
        return false;
    }
}

int run_test(bool mainProcess, const set<int>& case_set, const string command)
{
    int cases = 0, passed = 0;
    while (true)
    {
        if (next_line().find("--") != 0)
            break;
        vector<int> hunger;
        from_stream(hunger);
        vector<int> greed;
        from_stream(greed);
        int totalFood;
        from_stream(totalFood);
        next_line();
        int __answer;
        from_stream(__answer);

        cases++;
        if (case_set.size() > 0 && case_set.find(cases - 1) == case_set.end())
            continue;

        cout << "  Testcase #" << cases - 1 << " ... ";
        if (do_test(hunger, greed, totalFood, __answer))
        {
            passed++;
        }
    }
    if (mainProcess)
    {
        cout << endl
            << "Passed : " << passed << "/" << cases << " cases" << endl;
        int T = time(NULL) - 1643778711;
        double PT = T / 60.0, TT = 75.0;
        cout << "Time   : " << T / 60 << " minutes " << T % 60 << " secs" << endl;
        cout << "Score  : " << 500 * (0.3 + (0.7 * TT * TT) / (10.0 * PT * PT + TT * TT)) << " points" << endl;
    }
    return 0;
}

int main(int argc, char* argv[])
{
    cout.setf(ios::fixed, ios::floatfield);
    cout.precision(2);
    set<int> cases;
    bool mainProcess = true;
    for (int i = 1; i < argc; ++i)
    {
        if (string(argv[i]) == "-")
        {
            mainProcess = false;
        }
        else
        {
            cases.insert(atoi(argv[i]));
        }
    }
    if (mainProcess)
    {
        cout << "Badgers (500 Points)" << endl
            << endl;
    }
    return run_test(mainProcess, cases, argv[0]);
}
// CUT end
